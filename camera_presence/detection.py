# Face detection and tracking based on https://github.com/gdiepen/face-recognition
# https://answers.opencv.org/question/207286/why-imencode-taking-so-long/
import cv2
import dlib
import time
import json
# from multiprocessing import Process
from threading import Thread

import paho.mqtt.client as mqtt
import base64

class Detection:
    def __init__(self, name, queue, config):
        self.name = name
        self.queue = queue

        # feed
        self.feed_url = config['capture']['path']
        self.fps = config['capture']['fps']

        #web
        self.write_to_flask_enabled = config['web']['enabled']
        self.web_resolution = config['web']['resolution']
        self.web_fps = config['web']['fps']

        #mqtt
        self.publish_to_mqtt_enabled = config['mqtt']['enabled']
        self.mqtt_server_address = config['mqtt']['broker_host']
        self.mqtt_server_port = config['mqtt']['broker_port']

        #write to disk
        self.write_image_enabled = config['write_to_disk']['enabled']
        self.write_image_output = config['write_to_disk']['path']

        #Statistics
        self.time_expected = 60 / self.fps
        self.total_delay = 0
        self.statistics = {
            "total_time": 0,
            "time_get_frame": 0,
            "time_resizing": 0,
            "time_grey": 0,
            "time_losing_tracking": 0,
            "time_detecting": 0,
            "time_to_mqtt": 0,
            "time_to_flask": 0,
            "time_to_write": 0,
        }

        self.frame_counter = 0

        self.current_face_id = 0
        self.face_trackers = {}

    def process_feed(queue):
        #Clean the feed buffer
        while not queue.empty():
            queue.get()

        while True:
            while not queue.empty():
                frame = queue.get()
                yield frame

    def publish_to_mqtt(self, image):
        print('Publishing to Mqtt Cropped shape')
        print(image.shape)

        et, crop_img = cv2.imencode('.jpg', image)

        event = {
            'event': 'new_face',
            'data': {
                'image': '',
            }
        }

        event['data']['image'] = base64.b64encode(crop_img).decode('utf-8')

        self.client.publish(
            'camera_presence/'+ self.name + '/feed',
            json.dumps(event),
            qos=0,
            retain=False
        )

    def write_image(self, image):
        print('Writing Cropped shape')
        print(image.shape)

        cv2.imwrite(self.write_image_output + str(time.time()) + '.png', image)

    def write_to_flask(self, image, buffer):
        ret, buffer2 = cv2.imencode('.png', image)

        buffer.put(b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' + buffer2.tobytes() + b'\r\n')

    def worker(self):
        capture = cv2.VideoCapture(self.feed_url)

        rc, feedTestSize = capture.read()
        print('Feed shape')
        print(feedTestSize.shape)

        rectangleColor = (0, 165, 255)

        faceCascade = cv2.CascadeClassifier(cv2.data.haarcascades + '/haarcascade_frontalface_alt2.xml')

        self.statistics['total_time'] = time.time()
        while True:
            #---------STARTING TIMER--------------------
            time_get_frame = time.time()
            #Capture next frame
            rc, fullSizeBaseImage = capture.read()

            self.frame_counter += 1
            self.statistics['time_get_frame'] += time.time() - time_get_frame
            #---------ENDING TIMER----------------------

            #Print statistics report
            if self.frame_counter % 60 == 0:
                self.statistics['total_time'] = time.time() - self.statistics['total_time']

                self.total_delay += self.statistics['total_time'] - self.time_expected
                total_time_tracked = 0;

                print('\nExpected time = ', self.time_expected)
                print(self.statistics)

                self.statistics['time_get_frame'] = 0
                for key in self.statistics:
                    if key != 'total_time':
                        total_time_tracked += self.statistics[key]
                        self.statistics[key] = 0

                print('Number of trackers = ', len(self.face_trackers))
                print('Total time tracked = ', total_time_tracked)
                print('Total delay = ', self.total_delay)

                self.statistics['total_time'] = time.time()

            #---------STARTING TIMER--------------------
            time_resizing = time.time()

            #Resize the image to 320x240
            baseImage = fullSizeBaseImage

            self.statistics['time_resizing'] += time.time() - time_resizing
            #---------ENDING TIMER----------------------

            # resultImage = baseImage.copy()

            #---------STARTING TIMER--------------------
            time_losing_tracking = time.time()
            fidsToDelete = []
            for fid in self.face_trackers.keys():
                trackingQuality = self.face_trackers[fid].update(baseImage)

                #If the tracking quality is not good enough, we must delete
                #this tracker
                if trackingQuality < 7:
                    fidsToDelete.append(fid)

            for fid in fidsToDelete:
                print("Removing fid " + str(fid) + " from list of trackers")
                self.face_trackers.pop(fid , None)

            self.statistics['time_losing_tracking'] += time.time() - time_losing_tracking
            #---------ENDING TIMER----------------------

            if (self.frame_counter % 5) == 0:
                #For the face detection, we need to make use of a gray
                #colored image so we will convert the baseImage to a
                #gray-based image
                #---------STARTING TIMER--------------------
                time_grey = time.time()
                gray = cv2.cvtColor(baseImage, cv2.COLOR_BGR2GRAY)

                self.statistics['time_grey'] += time.time() - time_grey
                #---------ENDING TIMER----------------------

                #Now use the haar cascade detector to find all faces
                #in the image
                #---------STARTING TIMER--------------------
                time_detecting = time.time()

                faces = faceCascade.detectMultiScale(
                    gray,
                    scaleFactor=1.8,
                    minNeighbors=5,
                    minSize=(60, 60),
                    # maxSize=(120, 120),
                )

                self.statistics['time_detecting'] += time.time() - time_detecting
                #---------ENDING TIMER----------------------

                #We need to convert it to int here because of the
                #requirement of the dlib tracker. If we omit the cast to
                #int here, you will get cast errors since the detector
                #returns numpy.int32 and the tracker requires an int
                for (_x,_y,_w,_h) in faces:
                    x = int(_x)
                    y = int(_y)
                    w = int(_w)
                    h = int(_h)

                    if self.write_to_flask_enabled:
                        cv2.rectangle(baseImage, (x-10, y-20), (x + w+10 , y + h+20), rectangleColor, 2)

                    #calculate the centerpoint
                    x_bar = x + 0.5 * w
                    y_bar = y + 0.5 * h

                    matchedFid = None

                    #Now loop over all the trackers and check if the
                    #centerpoint of the face is within the box of a
                    #tracker
                    for fid in self.face_trackers.keys():
                        tracked_position =  self.face_trackers[fid].get_position()

                        t_x = int(tracked_position.left())
                        t_y = int(tracked_position.top())
                        t_w = int(tracked_position.width())
                        t_h = int(tracked_position.height())

                        #calculate the centerpoint
                        t_x_bar = t_x + 0.5 * t_w
                        t_y_bar = t_y + 0.5 * t_h

                        #check if the centerpoint of the face is within the
                        #rectangleof a tracker region. Also, the centerpoint
                        #of the tracker region must be within the region
                        #detected as a face. If both of these conditions hold
                        #we have a match
                        if ( ( t_x <= x_bar   <= (t_x + t_w)) and
                             ( t_y <= y_bar   <= (t_y + t_h)) and
                             ( x   <= t_x_bar <= (x   + w  )) and
                             ( y   <= t_y_bar <= (y   + h  ))):
                            matchedFid = fid

                    if matchedFid is None:
                        print("Creating new tracker " + str(self.current_face_id))

                        #Create and store the tracker
                        tracker = dlib.correlation_tracker()
                        tracker.start_track (
                            baseImage,
                            dlib.rectangle(x-10, y-20, x+w+10,y+h+20)
                        )

                        self.face_trackers[self.current_face_id] = tracker

                        # verify limits of the image
                        left = y - 80 if y - 80 > 0 else 0
                        right = y + h + 80 if y + h + 80 < fullSizeBaseImage.shape[0] else fullSizeBaseImage.shape[0]
                        top = x - 60 if x - 60 > 0 else 0
                        down = x + w + 60 if x + w + 60 < fullSizeBaseImage.shape[1] else fullSizeBaseImage.shape[1]
                        

                        crop_img = fullSizeBaseImage[left:right, top:down]

                        self.current_face_id += 1

                        #---------STARTING TIMER--------------------
                        time_to_mqtt = time.time()
                        if self.publish_to_mqtt_enabled:
                            self.publish_to_mqtt(crop_img)
                        self.statistics['time_to_mqtt'] += time.time() - time_to_mqtt
                        #---------ENDING TIMER--------------------

                        #---------STARTING TIMER--------------------
                        time_to_write = time.time()
                        if self.write_image_enabled:
                            try:
                                self.write_image(crop_img)
                            except:
                                print('error')
                            # self.write_image(crop_img)
                        self.statistics['time_to_write'] += time.time() - time_to_write
                        #---------ENDING TIMER--------------------

            if self.write_to_flask_enabled and self.frame_counter % self.web_fps:
                #---------STARTING TIMER--------------------
                time_to_flask = time.time()

                baseImage = cv2.resize(fullSizeBaseImage, tuple(self.web_resolution))

                self.write_to_flask(baseImage, self.queue)

                self.statistics['time_to_flask'] += time.time() - time_to_flask
                #---------ENDING TIMER--------------------

    def run(self):
        self.client = mqtt.Client()
        self.client.connect(self.mqtt_server_address, self.mqtt_server_port, 60)

        t1 = Thread(target=self.worker, daemon=True)
        t1.start()
        # p1 = Process(target=self.worker)
        # p1.start()
